#Timesheet

##Intro
Software responsável por controlar as batidas de ponto da empresa ACME desenvolvido para a EDS

### Pré-requisitos
* node v >= 8.9.1
* npm v >= 5.8.0

### Features
* Filtro de funcionário
* Pretty output

##Instalação
```
git clone git@bitbucket.org:augusto3691/timesheet.git timesheet
cd timesheet
npm install
```

##Uso
Coloque o arquivo de entradas `timeclock_entries.json` na pasta raiz do projeto e execute o seguinte comando:

```
node index.js -e <timeclock_entries.json> [-i] [-p]
```
Onde:

-e: nome do arquivo

-i: numero do pis do funcionário (opcional)

-p: configura saida pretty (opcional)