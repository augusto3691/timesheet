const _ = require('lodash');
const config = require('./config/config');
const employees = config.employees;
const stdio = require('stdio');
const fs = require('fs');
const moment = require('moment');
const chalk = require('chalk');
const momentDurationFormatSetup = require('moment-duration-format');
const prettyjson = require('prettyjson');
const log = console.log;

let ops = stdio.getopt({
  'entries': { key: 'e', args: 1, description: 'Time clock entries file', mandatory: true },
  'pis_number': { key: 'i', args: 1, description: 'Pis number of user' },
  'pretty': { key: 'p', args: 1, description: 'Pretty output' },
});

//Validação para ver se o arquivo esta na pasta
if (!fs.existsSync(ops.entries)) {
  log(chalk.red('Arquivo não encontrado'));
  process.exit();
}

let timeEntries = JSON.parse(fs.readFileSync(ops.entries, 'utf8'));
let output = [];

_.forEach(employees, function (employee) {

  output.push({
    pis_number: employee.pis_number,
    summary: {
      balance: calcSummary(employee),
    },
    history: hydrateDayBalance(getChunkDay(employee)),
  });

});

//Filtro caso usário passe o parâmetro i (pis_number)
if (ops.pis_number) {
  output = output.filter(function (employee) {
    return employee.pis_number == ops.pis_number;
  });
}

//Validação para pretty output
if (ops.pretty) {
  log(prettyjson.render(output, {
    keysColor: 'yellow',
    dashColor: 'yellow',
    stringColor: 'white'
  }));
} else {
  log(JSON.stringify(output, null, ' '));
}

// Functions

/**
 * @param {number} pis_number Numero Pis de identificação do funcionário
 */
function getTimeEntry(pis_number) {
  let filtered = timeEntries.filter(function (entry) {
    return entry.pis_number == pis_number;
  });

  return filtered[0].entries;
}

/**
 * @param {object} employee Objeto Entidade do funcionário
 */
function getChunkDay(employee) {
  let outputChunk = [];
  let entries = getTimeEntry(employee.pis_number);
  let chunk = _.groupBy(entries, function (entry_day) {
    return moment(entry_day).startOf('day').format('Y-M-D');
  });

  _.forEach(chunk, function (entries, day) {
    outputChunk.push({
      day: day,
      balance: calcDayBalance(entries, employee.workload[0].minimum_rest_interval_in_minutes, employee.workload[0].workload_in_minutes)
    });
  })

  return outputChunk;
}

/**
 * @param {Array} entries Array com todas as batidas de ponto
 * @param {number} restIntervalMax Tempo em minutos máximo que o funcionário pode fazer de descanso
 * @param {number} workload Tempo em minutos que o funcionário deve trabalhar por dia
 */
function calcDayBalance(entries, restIntervalMax, workload) {
  /*
  - Calcular o descanço dele somente se ele tiver batido 4 vezes o ponto senão conta o descanço que ele deveria fazer que consta no workload
  - O intervalo de descanço é retirar o primeiro e ultimo elemento do array e contar o intervalo entre os dois restantes
  - Se o descanço dele for maior que o que consta no workload, considerar o do workload como máximo de descanço, não descontar horas nesse caso
  */

  let timeInterval = moment.duration(moment(_.last(entries)).diff(moment(_.head(entries))));

  if (entries.length == 4) {
    entries.shift();
    entries.pop();
    let restIntervalPartial = moment.duration(moment(_.last(entries)).diff(moment(_.head(entries))));
    restInterval = (restIntervalPartial.asMinutes() > restIntervalMax ? restIntervalMax : restIntervalPartial.asMinutes());
  } else {
    restInterval = restIntervalMax;
  }

  let balance = timeInterval.asMinutes() - restInterval - workload;
  return balance;
}

/**
 * @param {object} employee Objeto Entidade do funcionário
 */
function calcSummary(employee) {
  let outputSummary = [];
  let entries = getTimeEntry(employee.pis_number);
  let chunk = _.groupBy(entries, function (entry_day) {
    return moment(entry_day).startOf('day').format('Y-M-D');
  });

  _.forEach(chunk, function (entries) {
    outputSummary.push(
      calcDayBalance(entries, employee.workload[0].minimum_rest_interval_in_minutes, employee.workload[0].workload_in_minutes));
  })

  let summary = moment.duration(_.sum(outputSummary), "minutes").format('HH:mm', { trim: false });
  return summary;
}

/**
 * @param {Array} balance Array pré definido na função calcDayBalance para ser hidratado
 */
function hydrateDayBalance(balance) {
  _.forEach(balance, function (entry) {
    entry.balance = moment.duration(entry.balance, "minutes").format('HH:mm', { trim: false });
  });

  return balance;
}